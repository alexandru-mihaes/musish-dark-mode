![Preview](assets/images/preview.png)

Dark mode for [Musish](https://musi.sh/) - the unofficial open source [Apple Music](https://www.apple.com/apple-music/) web app

## Installation
Install [Stylus](https://add0n.com/stylus.html) for either [Firefox](https://addons.mozilla.org/en-US/firefox/addon/styl-us/) or [chromium-based web browsers](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne/).

